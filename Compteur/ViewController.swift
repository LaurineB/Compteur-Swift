//
//  ViewController.swift
//  Compteur
//
//  Created by Laurine Baillet on 07/11/2016.
//  Copyright © 2016 ESTIAM. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UINavigationBarDelegate {
    
    var i : Int = 0

    @IBOutlet weak var monTexte: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func `switch`(_ sender: AnyObject) {
        monTexte.text = String(i)
        i = i+1
    }
    @IBOutlet weak var aller: UIButton!
    @IBAction func goToSecond(_ sender: AnyObject) {
        let second = self.storyboard?.instantiateViewController(withIdentifier: "Second") as! SecondViewController
        
        //second.entierAAfficher = i;
        
        self.navigationController?.pushViewController(second, animated: true)
    }

}

